/**
 * Created by studentwtep on 7/1/2017.
 */
var myApp = angular.module('myApp',[]);
myApp.controller('AppController', function ($scope) {
    $scope.product={
        name: 'Apple',
        price: 50
    }      ;
    $scope.fru=['Apple','Banana','Grapes'];
    $scope.products= [
        {name: 'Alaska',price: 900, qty :3, pound : 2, imagepath: 'images/Dutch-Truffle38292.jpg'},
        {name: 'Chocolate Fudge',price: 800, qty :3, pound : 2 , imagepath: 'images/service_5787d7ccbc46c.jpg'}];
    $scope.new={}
    $scope.addProduct=function()
    {
        $scope.new.imagepath='images/Dutch-Truffle38292.jpg';
        $scope.products.push($scope.new);
    };

    $scope.cart = {};
    $scope.totalprice = {};
    $scope.addtocart = function (item){
        item.qty = 1;
        $scope.totalprice = $scope.totalprice + item;
        $scope.cart.push(item);


    }

});
